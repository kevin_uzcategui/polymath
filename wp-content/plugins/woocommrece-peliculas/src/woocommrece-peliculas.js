jQuery( function( $ ) {

        
        $(".select-peliculas").imagepicker({
            hide_select : true,
            show_label  : true
        });
        
        
        var state_scroll_pelicula = true;
        
        /*Right*/
        $(document.body).on("click", ".right-bootom-pelicula", function() {
            
            if(state_scroll_pelicula){
                
                state_scroll_pelicula = false;
                
                jQuery(".thumbnails.image_picker_selector").animate({ 
                    scrollLeft: jQuery('.thumbnails.image_picker_selector').scrollLeft() + 160
                }, 
                300, 
                function() {
                    state_scroll_pelicula = true;
                });
            }
            
        });
        
        /*left*/
        $(document.body).on("click", ".left-bootom-pelicula", function() {
           
            if(state_scroll_pelicula){
                
                state_scroll_pelicula = false;       
           
                jQuery(".thumbnails.image_picker_selector").animate({ 
                    scrollLeft: jQuery('.thumbnails.image_picker_selector').scrollLeft() - 160
                }, 
                300,
                function() {
                    state_scroll_pelicula = true;
                });
                
            }    
        });            
            
        

        /*cambio de diseño de botom*/    
        $(".thumbnails.image_picker_selector").scroll(function(){
            
            $(".thumbnails.image_picker_selector").scrollLeft();
        
            
        });    


} );
